FROM node:18
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app
COPY --chown=node:node ./app.js ./
COPY --chown=node:node ./package.json ./
COPY --chown=node:node ./package-lock.json ./
COPY --chown=node:node ./node-config ./
COPY --chown=node:node ./node-services ./
COPY --chown=node:node ./node-clients ./
COPY --chown=node:node ./vue/dist ./public
USER node
RUN npm install

CMD ["node", "app.js"]