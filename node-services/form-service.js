import { Router } from 'express';
import { getKeycloak } from './keycloak-config.js';
import SparqlClient from './sparql-client.js'
import { v4 as uuidv4 } from 'uuid';

class FormService {
    #router = Router();
    #keycloak = getKeycloak();
    #configSparqlClient = new SparqlClient('config');
    #dataSparqlClient = new SparqlClient();

    // ENDPOINT /api/forms/*
    constructor() {
        this.#router.get('/get-all'/*, this.#keycloak.protect('admin')*/, (req, res) => {
            this.#configSparqlClient.query(`
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX shacl: <http://www.w3.org/ns/shacl#>

                SELECT * 
                FROM <http://shapes.lincsproject.ca>
                WHERE {
                    ?form rdf:type shacl:NodeShape .
                }
            `).then((response) => {
                let forms = [];

                for (let row of response.data.results.bindings) {
                    let formName = row.form.value.split('/');
                    forms.push({
                        name: formName[formName.length - 1],
                        uri: row.form.value,
                    });
                }

                res.send(forms);
            }).catch((e) => {
                res.status(500).send("FAILURE");
                console.error(e);
            });
        });

        this.#router.get('/get-form'/*, this.#keycloak.protect('admin')*/, (req, res) => {
            let report = req.query.report;
            let entity = req.query.entity;

            this.#configSparqlClient.query(`
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX shacl: <http://www.w3.org/ns/shacl#>

                SELECT * 
                FROM <http://shapes.lincsproject.ca>
                WHERE {
                    <` + report + `> shacl:property ?property .
                    ?property shacl:path ?path .
                    OPTIONAL {
                        ?property shacl:class ?class .
                    }
                    OPTIONAL {
                        ?property shacl:name ?name .
                    }
                    OPTIONAL {
                        ?property shacl:order ?order .
                    }
                    BIND(COALESCE(?order, 10000) AS ?fieldOrder)
                } ORDER BY ASC(?fieldOrder)
            `).then((response) => {
                let form = [];

                for (let row of response.data.results.bindings) {
                    let property = {
                        name: '',
                        property: row.property.value,
                        path: row.path.value,
                        class: '',
                        datatype: '',
                        value: ''
                    };

                    if (row.name != null) {
                        property.name = row.name.value;
                    }

                    if (row.class != null) {
                        property.class = row.class.value;
                    } else {
                        property.datatype = 'xsd:string';
                    }
                    
                    form.push(property);
                }

                if (entity != '') {
                    this.getEntityInfo(form, entity, res);
                } else {
                    entity = "http://id.lincsproject.ca/" + uuidv4();
                    res.send({ entity: entity, form: form });
                }
            }).catch((e) => {
                res.status(500).send("FAILURE");
                console.error(e);
            });
        });

        this.#router.post('/validate', this.#keycloak.protect('admin'), (req, res) => {
            res.send("Admin Endpoint");
        });

        this.#router.post('/submit'/*, this.#keycloak.protect('admin')*/, (req, res) => {
            let query = `
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                INSERT DATA { GRAPH <http://graph.lincsproject.ca/` + req.body.graph + `> {
            `;
            for (let field of req.body.form) {
                if (field.value != '') {
                    query += "<" + req.body.entity + "> <" + field.path + "> ";

                    if (field.class != '') {
                        query += "<" + field.value + "> .";
                    } else {
                        query += "\"" + field.value + "\"^^" + field.datatype + " .";
                    }
                }
            }
            query += "} }";

            this.#dataSparqlClient.update(query).then(() => {
                res.send("SUCCESS");
            }).catch(() => {
                res.sendStatus(500);
            });
        });

        this.#router.post('/delete', this.#keycloak.protect('admin'), (req, res) => {
            res.send("Admin Endpoint");
        });
    }

    getRouter() {
        return this.#router;
    }

    getEntityInfo(form, entity, res) {
        this.#configSparqlClient.query(`
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

            SELECT * 
            FROM <http://shapes.lincsproject.ca>
            WHERE {
                <` + entity + `> ?predicate ?object .
            }
        `).then((response) => {
            for (let row of response.data.results.bindings) {
                let property = form.find(prop => {
                    return prop.path === row.predicate.value;
                });   
                
                if (property != null) {
                    if (property.value == '') {
                        property.value = row.object.value;
                    } else {
                        let clonedProperty = JSON.parse(JSON.stringify(property));
                        clonedProperty.value = row.object.value;
                        form.push(clonedProperty);
                    }
                }
            }

            res.send({ entity: entity, form: form });
        }).catch((e) => {
            res.status(500).send("FAILURE");
            console.error(e);
        });
    }
}

export default FormService;