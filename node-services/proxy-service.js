import { Router } from 'express';
import { getKeycloak } from './keycloak-config.js';
import axios from 'axios';
import Configuration from './global-config.js';

class ProxyService {
    #router = Router();
    #keycloak = getKeycloak();

    // ENDPOINT /proxy/*
    constructor() {
        this.#router.post('/config-sparql-endpoint'/*, this.#keycloak.protect('admin')*/, async function(req, res){
            const post = await axios.post(Configuration.config.endpoint, req, {
                responseType: 'stream'
            }).catch(() => {
                res.sendStatus(500);
            });
        
            if (post != null) {
                post.data.pipe(res);
            }
        });
        
        this.#router.post('/data-sparql-endpoint'/*, this.#keycloak.protect('admin')*/, async function(req, res){
            const post = await axios.post(Configuration.data.endpoint, req, {
                responseType: 'stream'
            }).catch(() => {
                res.sendStatus(500);
            });
        
            if (post != null) {
                post.data.pipe(res);
            }
        });
    }

    getRouter() {
        return this.#router;
    }
}

export default ProxyService;