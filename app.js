import express from 'express';
import session, { MemoryStore } from 'express-session';
import cors from 'cors';

import ProxyService from './proxy-service.js';
import FormService from './form-service.js';
import { initKeycloak } from './keycloak-config.js';
import Configuration from './global-config.js';

let config = new Configuration();
config.startup();

const app = express();
const port = Configuration.app.port;

app.use(cors());
app.use(express.json());

var memoryStore = new MemoryStore();
app.use(session({
    secret: Configuration.keycloak.secret,
    resave: false,
    saveUninitialized: true,
    store: memoryStore
}));

const keycloak = initKeycloak(memoryStore);
app.use(keycloak.middleware());

app.use('/proxy', new ProxyService().getRouter());
app.use('/api/forms', new FormService().getRouter());

app.use(express.static('public'));

app.get('/keycloak', (req, res) => {
    res.send({
        url: Configuration.keycloak.url,
        realm: Configuration.keycloak.realm,
        clientId: Configuration.keycloak.clientFrontend
    });
});

app.listen(port, "0.0.0.0", () => {
  console.log(`App listening on port ${port}`);
});