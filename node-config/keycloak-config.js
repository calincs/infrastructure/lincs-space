import Keycloak from 'keycloak-connect';
import Configuration from './global-config.js';

let _keycloak;

var keycloakConfig = {
    clientId: Configuration.keycloak.clientBackend,
    bearerOnly: true,
    serverUrl: Configuration.keycloak.url,
    realm: Configuration.keycloak.realm,
    credentials: {
        secret: Configuration.keycloak.secret
    }
};

function initKeycloak(memoryStore) {
    if (_keycloak) {
        console.warn("Trying to init Keycloak again!");
        return _keycloak;
    } else {
        console.log("Initializing Keycloak...");
        _keycloak = new Keycloak({ store: memoryStore }, keycloakConfig);
        return _keycloak;
    }
}

function getKeycloak() {
    if (!_keycloak){
        console.error('Keycloak has not been initialized. Please called init first.');
    } 
    return _keycloak;
}

export {
    initKeycloak,
    getKeycloak
};