import fs from "fs";
import ShaclClient from "./shacl-client.js";
import SparqlClient from "./sparql-client.js";

class Configuration {
    #configFields = {
        app: [
            'port'
        ],
        config: [
            'endpoint',
            'username',
            'password'
        ],
        data: [
            'endpoint',
            'username',
            'password'
        ]
    }

    startup() {
        // Load global-config.json
        let data = fs.readFileSync('./global-config.json', { encoding: 'utf8', flag: 'r'});
        if (data != null) {
            data = JSON.parse(data);
            for (const [key, value] of Object.entries(this.#configFields)) {
                for (const field of value) {
                    if (data[key][field] != null) {
                        Configuration[key][field] = data[key][field];
                    } else {
                        Configuration.configPrompt = true;
                    }
                }
            }
        } else {
            Configuration.configPrompt = true;
        }

        // Add preset configuration graphs
        let sparqlClient = new SparqlClient('config');

        let filenames = fs.readdirSync('./rdf/configurations');
        filenames.forEach(file => {
            sparqlClient.uploadFile('./rdf/configurations/' + file, file);
        });

        filenames = fs.readdirSync('./rdf/shacl');
        filenames.forEach(file => {
            sparqlClient.uploadFile('./rdf/shacl/' + file, 'http://shapes.lincsproject.ca', false);
        });

        // Add preset data graphs
        sparqlClient = new SparqlClient();

        filenames = fs.readdirSync('./rdf/ontologies');
        filenames.forEach(file => {
            sparqlClient.uploadFile('./rdf/ontologies/' + file, file);
        });

        filenames = fs.readdirSync('./rdf/vocabularies');
        filenames.forEach(file => {
            sparqlClient.uploadFile('./rdf/vocabularies/' + file, file);
        });

        /*filenames = fs.readdirSync('./rdf/data');
        filenames.forEach(file => {
            sparqlClient.uploadFile('./rdf/data/' + file, file);
        });*/
    }

    static configPrompt = false;

    static app = {
        port: 0
    }

    static keycloak = {
        clientBackend: process.env.KEYCLOAK_CLIENT_BACKEND,
        clientFrontend: process.env.KEYCLOAK_CLIENT_FRONTEND,
        url: process.env.KEYCLOAK_URL,
        realm: process.env.KEYCLOAK_REALM,
        secret: process.env.KEYCLOAK_SECRET
    }

    static config = {
        endpoint: '',
        username: '',
        password: ''
    }

    static data = {
        endpoint: '',
        username: '',
        password: ''
    }
}
   
export default Configuration;