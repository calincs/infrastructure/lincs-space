import axios from "axios";
import fs from "fs";
import concat from "concat-stream";
import FormData from "form-data";
import Configuration from "./global-config.js";

class SparqlClient {
    constructor(triplestore='data') {
        this.triplestore = triplestore;
    }

    uploadFile(filepath, graph, checkOverwrite=true) {
        var vm = this;
        var scrubbedGraph = decodeURIComponent(graph.split('.split')[0].replace('.trig', ''));
        this.query("ASK WHERE { GRAPH <" + scrubbedGraph + "> { ?s ?p ?o } }").then(function (response) {
            if (!checkOverwrite || !response.data.boolean) {
                const fd = new FormData();
                fd.append("file", fs.createReadStream(filepath));
                fd.pipe(concat({encoding: 'buffer'}, data => {
                    axios.post(Configuration[vm.triplestore].endpoint, data, {
                        headers: fd.getHeaders(),
                        auth: {
                            username: Configuration[vm.triplestore].username,
                            password: Configuration[vm.triplestore].password
                        }
                    }).catch(function(e) {
                        console.error("FAILED TO UPLOAD: " + scrubbedGraph);
                    });
                }));
            } else {
                console.log(scrubbedGraph + " already exists in a graph, skipping upload.")
            }
        }).catch(function(e) {
            console.error("FAILED TO UPLOAD: " + scrubbedGraph);
        });
    }

    query(query) {
        return axios.get(Configuration[this.triplestore].endpoint + "?query=" + encodeURIComponent(query));
    }

    update(query) {
        return axios.post(Configuration[this.triplestore].endpoint + "?update=" + encodeURIComponent(query));
    }

    getNode() {
        // future planning
    }

    getGraph() {
        // future planning
    }
}

export default SparqlClient;