import fs from "fs";
import DataFactory from "rdf-ext";
import ParserN3 from "@rdfjs/parser-n3";
import SHACLValidator from "rdf-validate-shacl";
import Readable from "readable-stream";

class ShaclClient {
    constructor(graph) {
        const vm = this;
        this.parser = new ParserN3({ DataFactory });

        const stream = fs.createReadStream("./rdf/shacl/" + encodeURIComponent(graph) + ".trig");

        const promise = new Promise((resolve) => {
            DataFactory.dataset().import(this.parser.import(stream)).then(function(shapes) {
                vm.validator = new SHACLValidator(shapes, { DataFactory });
                resolve(vm);
            });
        });

        return promise;
    }

    validate(triples) {
        const vm = this;

        const input = new Readable({
            read: () => {
                input.push(triples);
                input.push(null);
            }
        });

        const promise = new Promise((resolve) => {
            DataFactory.dataset().import(vm.parser.import(input)).then(function(data) {
                resolve(vm.validator.validate(data));
            });
        });

        return promise;
    }
}

export default ShaclClient;